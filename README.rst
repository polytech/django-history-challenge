Django History Challenge
========================

Task Description
----------------

You are given a simple Django project that can model products and quality checks performed during the production of said products.
Changes to these models can be made either using the Django admin interface or a REST API.

Your Task:

* Please extend the project so it is able to track (value) changes done to the model instances.
* This shall include

  * Who did a change
  * When was a change done
  * Which fields were changed (including the differences)

* Changes shall be recorded regardless of whether a model instance was changed via the admin interface or the REST API.
* Please add a way to look up the history of a specific model instance. It is your choice whether you add a REST endpoint or adapt the admin interface.

Django Project Description
--------------------------

The project consists of two app (products and quality_checks). The products app contains models for two different measurement devices and their subcomponents
as well as two different types of sensors.
All models inherit from a common ``BaseProduct`` model, which provides common fields, like a ``date_created`` or a ``serial_number``.
The more specific products can add additional fields, like ForeignKeys to sub-components in case of the devices or numeric characteristics in case of the
sensors.

For each product there is a corresponding model in the quality_checks app. During production instances of these models would be created, whenever a component
is tested by production staff.
They again inherit from a common ``BaseQualityCheck`` model providing common fields.

Test Data
---------

A ``test_data.json`` is provided, to quickly start database with some users and data.

There is one superuser provided in the test data, named ``admin``, as well as two normal users named ``alice`` and ``bob``.
All users' passwords are set to ``SuperSecret``.

To load the data first migrate an empty database and then load the data using Django's manage.py.::

    > ./manage.py migrate
    > ./manage.py loaddata test_data.json
