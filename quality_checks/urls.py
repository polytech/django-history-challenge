from django.urls import path, include
from rest_framework.routers import DefaultRouter

from quality_checks.api import modelviewset_factory
from quality_checks.models import (
    BaseQualityCheck,
    PCBAQualityCheck,
    OpticalMeasurementChannelQualityCheck,
    OpticalMeasurementDevice2ChannelQualityCheck,
    OpticalMeasurementDevice4ChannelQualityCheck,
)

router = DefaultRouter()

router.register(f'{BaseQualityCheck._meta.model_name}', modelviewset_factory(BaseQualityCheck))
router.register(f'{PCBAQualityCheck._meta.model_name}', modelviewset_factory(PCBAQualityCheck))
router.register(f'{OpticalMeasurementChannelQualityCheck._meta.model_name}', modelviewset_factory(OpticalMeasurementChannelQualityCheck))
router.register(f'{OpticalMeasurementDevice2ChannelQualityCheck._meta.model_name}', modelviewset_factory(OpticalMeasurementDevice2ChannelQualityCheck))
router.register(f'{OpticalMeasurementDevice4ChannelQualityCheck._meta.model_name}', modelviewset_factory(OpticalMeasurementDevice4ChannelQualityCheck))

urlpatterns = [
    path('', include(router.urls)),
]