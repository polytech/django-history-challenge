from typing import Type

from rest_framework.serializers import ModelSerializer
from rest_framework.viewsets import ModelViewSet

from quality_checks.models import BaseQualityCheck


def modelviewset_factory(quality_check_model: Type[BaseQualityCheck]) -> Type[ModelViewSet]:
    class QualityCheckSerializer(ModelSerializer):
        class Meta:
            model = quality_check_model
            fields = '__all__'

    class QualityCheckModelViewSet(ModelViewSet):
        queryset = quality_check_model.objects.all()
        serializer_class = QualityCheckSerializer

    return QualityCheckModelViewSet

