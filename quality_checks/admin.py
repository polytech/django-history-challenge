from django.contrib import admin
from polymorphic.admin import PolymorphicParentModelAdmin, PolymorphicChildModelAdmin

from quality_checks.models import (
    BaseQualityCheck,
    PCBAQualityCheck,
    OpticalMeasurementChannelQualityCheck,
    OpticalMeasurementDevice2ChannelQualityCheck,
    OpticalMeasurementDevice4ChannelQualityCheck,
)


@admin.register(BaseQualityCheck)
class BaseProductModelAdmin(PolymorphicParentModelAdmin):
    base_model = BaseQualityCheck
    child_models = (
        PCBAQualityCheck,
        OpticalMeasurementChannelQualityCheck,
        OpticalMeasurementDevice2ChannelQualityCheck,
        OpticalMeasurementDevice4ChannelQualityCheck,
    )
    list_display = ("__str__", "date_created")


@admin.register(PCBAQualityCheck)
class PCBAQualityCheckModelAdmin(PolymorphicChildModelAdmin):
    model = PCBAQualityCheck


@admin.register(OpticalMeasurementChannelQualityCheck)
class OpticalMeasurementChannelQualityCheckModelAdmin(PolymorphicChildModelAdmin):
    model = OpticalMeasurementChannelQualityCheck


@admin.register(OpticalMeasurementDevice2ChannelQualityCheck)
class OpticalMeasurementDevice2ChannelQualityCheckModelAdmin(PolymorphicChildModelAdmin):
    model = OpticalMeasurementDevice2ChannelQualityCheck


@admin.register(OpticalMeasurementDevice4ChannelQualityCheck)
class OpticalMeasurementDevice4ChannelQualityCheckModelAdmin(PolymorphicChildModelAdmin):
    model = OpticalMeasurementDevice4ChannelQualityCheck
