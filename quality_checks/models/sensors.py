from django.db import models

from quality_checks.models.base import BaseQualityCheck


class StrainSensorQualityCheck(BaseQualityCheck):
    measured_wavelength = models.FloatField()


class TemperatureSensorQualityCheck(BaseQualityCheck):
    ambient_temperature = models.FloatField()

    measured_wavelength = models.FloatField()
    measured_temperature = models.FloatField()
