from django.db import models
from polymorphic.models import PolymorphicModel


class BaseQualityCheck(PolymorphicModel):
    date_created = models.DateTimeField(auto_now_add=True)
    date_edited = models.DateTimeField(auto_now=True)

    product = models.ForeignKey('products.BaseProduct', on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.__class__.__name__} of {self.product.serial_number}"
