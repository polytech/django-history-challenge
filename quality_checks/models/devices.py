from django.db import models

from quality_checks.models.base import BaseQualityCheck


class OpticalMeasurementDevice2ChannelQualityCheck(BaseQualityCheck):
    channel_1_deviation = models.FloatField()
    channel_2_deviation = models.FloatField()


class OpticalMeasurementDevice4ChannelQualityCheck(BaseQualityCheck):
    channel_1_deviation = models.FloatField()
    channel_2_deviation = models.FloatField()
    channel_3_deviation = models.FloatField()
    channel_4_deviation = models.FloatField()
