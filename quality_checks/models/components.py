from django.db import models

from quality_checks.models.base import BaseQualityCheck


class PCBAQualityCheck(BaseQualityCheck):
    optical_check = models.BooleanField(default=False)
    electrical_check = models.BooleanField(default=False)


class OpticalMeasurementChannelQualityCheck(BaseQualityCheck):
    optical_loss = models.FloatField()
