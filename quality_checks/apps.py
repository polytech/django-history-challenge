from django.apps import AppConfig


class QualityChecksConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'quality_checks'
