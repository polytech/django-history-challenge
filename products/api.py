from typing import Type

from rest_framework.serializers import ModelSerializer
from rest_framework.viewsets import ModelViewSet

from products.models import BaseProduct


def modelviewset_factory(product_model: Type[BaseProduct]) -> Type[ModelViewSet]:
    class ProductSerializer(ModelSerializer):
        class Meta:
            model = product_model
            fields = '__all__'

    class ProductModelViewSet(ModelViewSet):
        queryset = product_model.objects.all()
        serializer_class = ProductSerializer

    return ProductModelViewSet

