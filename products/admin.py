from django.contrib import admin
from polymorphic.admin import PolymorphicParentModelAdmin, PolymorphicChildModelAdmin

from products.models import BaseProduct, PCBA, OpticalMeasurementChannel, OpticalMeasurementDevice2Channel, OpticalMeasurementDevice4Channel


@admin.register(BaseProduct)
class BaseProductModelAdmin(PolymorphicParentModelAdmin):
    base_model = BaseProduct
    child_models = (PCBA, OpticalMeasurementChannel, OpticalMeasurementDevice2Channel, OpticalMeasurementDevice4Channel)
    list_display = ("serial_number", "date_created")


@admin.register(PCBA)
class PCBAModelAdmin(PolymorphicChildModelAdmin):
    model = PCBA

@admin.register(OpticalMeasurementChannel)
class OpticalMeasurementChannelModelAdmin(PolymorphicChildModelAdmin):
    model = OpticalMeasurementChannel

@admin.register(OpticalMeasurementDevice2Channel)
class OpticalMeasurementDevice2ChannelModelAdmin(PolymorphicChildModelAdmin):
    model = OpticalMeasurementDevice2Channel

@admin.register(OpticalMeasurementDevice4Channel)
class OpticalMeasurementDevice4ChannelModelAdmin(PolymorphicChildModelAdmin):
    model = OpticalMeasurementDevice4Channel

