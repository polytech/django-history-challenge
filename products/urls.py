from django.urls import path, include
from rest_framework.routers import DefaultRouter

from products.api import modelviewset_factory
from products.models import BaseProduct, PCBA, OpticalMeasurementChannel, OpticalMeasurementDevice2Channel, OpticalMeasurementDevice4Channel

router = DefaultRouter()

router.register(f'{BaseProduct._meta.model_name}', modelviewset_factory(BaseProduct))
router.register(f'{PCBA._meta.model_name}', modelviewset_factory(PCBA))
router.register(f'{OpticalMeasurementChannel._meta.model_name}', modelviewset_factory(OpticalMeasurementChannel))
router.register(f'{OpticalMeasurementDevice2Channel._meta.model_name}', modelviewset_factory(OpticalMeasurementDevice2Channel))
router.register(f'{OpticalMeasurementDevice4Channel._meta.model_name}', modelviewset_factory(OpticalMeasurementDevice4Channel))

urlpatterns = [
    path('', include(router.urls)),
]