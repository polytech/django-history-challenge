from django.db import models

from products.models.base import BaseProduct


class OpticalMeasurementDevice2Channel(BaseProduct):
    channel_1 = models.ForeignKey("OpticalMeasurementChannel", on_delete=models.SET_NULL, null=True, related_name="+")
    channel_2 = models.ForeignKey("OpticalMeasurementChannel", on_delete=models.SET_NULL, null=True, related_name="+")

    board = models.ForeignKey("PCBA", on_delete=models.SET_NULL, null=True, related_name="+")


class OpticalMeasurementDevice4Channel(BaseProduct):
    channel_1 = models.ForeignKey("OpticalMeasurementChannel", on_delete=models.SET_NULL, null=True, related_name="+")
    channel_2 = models.ForeignKey("OpticalMeasurementChannel", on_delete=models.SET_NULL, null=True, related_name="+")
    channel_3 = models.ForeignKey("OpticalMeasurementChannel", on_delete=models.SET_NULL, null=True, related_name="+")
    channel_4 = models.ForeignKey("OpticalMeasurementChannel", on_delete=models.SET_NULL, null=True, related_name="+")

    board = models.ForeignKey("PCBA", on_delete=models.SET_NULL, null=True, related_name="+")
