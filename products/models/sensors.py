from django.db import models

from products.models.base import BaseProduct


class StrainSensor(BaseProduct):
    expected_wavelength = models.FloatField()


class TemperatureSensor(BaseProduct):
    expected_wavelength = models.FloatField()
    temperature_coefficient = models.FloatField()