from django.db import models
from polymorphic.models import PolymorphicModel


class BaseProduct(PolymorphicModel):
    date_created = models.DateTimeField(auto_now_add=True)
    date_edited = models.DateTimeField(auto_now=True)

    serial_number = models.CharField(max_length=32)

    def __str__(self):
        return f"{self.__class__.__name__}: {self.serial_number}"
